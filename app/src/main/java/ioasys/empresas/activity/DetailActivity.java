package ioasys.empresas.activity;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.Map;

import ioasys.empresas.R;
import ioasys.empresas.model.Enterprise;
import ioasys.empresas.model.EnterpriseDetailResponse;
import ioasys.empresas.rest.EnterpriseApiService;
import ioasys.empresas.utils.SharedPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://54.94.179.135:8090/api/v1/";

    private static Retrofit mRetrofit = null;
    private SharedPreferences mSharedPreferences = null;
    private int mParamID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSharedPreferences = new SharedPreferences();
        mParamID = getIntent().getIntExtra("ENTERPRISE_ID", 0);

        final ImageView imgDetailEnterprise = (ImageView) findViewById(R.id.imgDetailEnterprise);

        final TextView txtEnterpriseDescription = (TextView) findViewById(R.id.enterpriseDescription);
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        EnterpriseApiService enterpriseApiService = mRetrofit.create(EnterpriseApiService.class);

        Call<EnterpriseDetailResponse> call = enterpriseApiService.getEnterpriseDetails(mParamID, getHeaders(this));
        call.enqueue(new Callback<EnterpriseDetailResponse>() {
            @Override
            public void onResponse(Call<EnterpriseDetailResponse> call, Response<EnterpriseDetailResponse> response) {
                Enterprise enterprise = response.body().getEnterprise();
                getSupportActionBar().setTitle(enterprise.getEnterpriseName());
                txtEnterpriseDescription.setText(enterprise.getDescription());
                Glide.with(getApplication())
                        .load(enterprise.getPhoto())
                        .error(R.mipmap.img_lista)
                        .into(imgDetailEnterprise);
            }

            @Override
            public void onFailure(Call<EnterpriseDetailResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro! Verifique a conexão com a internet.", Toast.LENGTH_SHORT).show();

            }


        });
    }


    private Map<String, String> getHeaders(Context context) {
        Map<String, String> map = new HashMap<>();

        String token = mSharedPreferences.getValueString(context, mSharedPreferences.TOKEN);
        String uid = mSharedPreferences.getValueString(context, mSharedPreferences.UID);
        String client = mSharedPreferences.getValueString(context, mSharedPreferences.CLIENT);
        map.put("Content-Type", "application/json");
        map.put("access-token", token);
        map.put("uid", uid);
        map.put("client", client);

        return map;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

