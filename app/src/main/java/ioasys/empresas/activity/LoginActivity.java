package ioasys.empresas.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ioasys.empresas.R;
import ioasys.empresas.model.User;
import ioasys.empresas.model.UserResponse;
import ioasys.empresas.rest.EnterpriseApiService;
import ioasys.empresas.utils.SharedPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LoginActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://54.94.179.135:8090/api/v1/";


    private EditText mEmail;
    private EditText mPassword;
    private static Retrofit mRetrofit = null;
    SharedPreferences mSharedPreferences = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);

        Button mEmailSignInButton = (Button) findViewById(R.id.loginButton);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User(mEmail.getText().toString(), mPassword.getText().toString());
                login(user, getApplicationContext());
            }
        });

    }

    private void login(User user, final Context context) {

        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        EnterpriseApiService enterpriseApiService = mRetrofit.create(EnterpriseApiService.class);

        Call<UserResponse> call = enterpriseApiService.login(user);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.body() != null && response.body().getSuccess()) {

                    mSharedPreferences = new SharedPreferences();

                    String token = response.headers().get("access-token");
                    String uid = response.headers().get("uid");
                    String client = response.headers().get("client");

                    mSharedPreferences.save(context, true, mSharedPreferences.USER_AUTHENTICATED);
                    mSharedPreferences.save(context, token, mSharedPreferences.TOKEN);
                    mSharedPreferences.save(context, uid, mSharedPreferences.UID);
                    mSharedPreferences.save(context, client, mSharedPreferences.CLIENT);

                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "E-mail e/ou senha inválidos.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro! Verifique a conexão com a internet.", Toast.LENGTH_SHORT).show();
            }


        });

    }


}

