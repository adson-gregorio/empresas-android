package ioasys.empresas.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ioasys.empresas.R;
import ioasys.empresas.adapter.EnterprisesAdapter;
import ioasys.empresas.adapter.RecyclerViewOnClickListener;
import ioasys.empresas.model.Enterprise;
import ioasys.empresas.model.EnterpriseSearchResponse;
import ioasys.empresas.rest.EnterpriseApiService;
import ioasys.empresas.utils.SharedPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Fragment";
    public static final String BASE_URL = "http://54.94.179.135:8090/api/v1/";

    private static Retrofit mRetrofit;
    private RecyclerView mRecyclerView;
    private EnterprisesAdapter mEnterprisesAdapter;
    private SharedPreferences mSharedPreferences;
    private Toolbar mToolbar;
    private TextView mRvPlaceholder;
    private ImageView mImgLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        mToolbar.inflateMenu(R.menu.menu_main);

        mSharedPreferences = new SharedPreferences();

        mImgLogo = (ImageView) findViewById(R.id.imgLogo);
        mRvPlaceholder = (TextView) findViewById(R.id.rvPlaceholder);


        boolean isUserAuthenticated = mSharedPreferences.getValueBoolean(this, mSharedPreferences.USER_AUTHENTICATED);

        if (!isUserAuthenticated) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mEnterprisesAdapter = new EnterprisesAdapter(new ArrayList<Enterprise>(),
                R.layout.rv_item_search, getApplicationContext());
        mRecyclerView.setAdapter(mEnterprisesAdapter);

    }

    public void connectAndGetApiData(String query) {

        final Context context = getApplicationContext();

        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        mRvPlaceholder.setText("Pesquisando..");

        EnterpriseApiService enterpriseApiService = mRetrofit.create(EnterpriseApiService.class);

        Call<EnterpriseSearchResponse> call = enterpriseApiService.searchEnterprisesName(query, getHeaders(context));
        call.enqueue(new Callback<EnterpriseSearchResponse>() {
            @Override
            public void onResponse(Call<EnterpriseSearchResponse> call, Response<EnterpriseSearchResponse> response) {
                final List<Enterprise> enterprises = response.body().getEnterprises();

                if (!enterprises.isEmpty()) {
                    mRvPlaceholder.setVisibility(View.INVISIBLE);
                } else {
                    mRvPlaceholder.setText("Nenhum resultado encontrado.");
                }

                mEnterprisesAdapter = new EnterprisesAdapter(enterprises, R.layout.rv_item_search, context);
                mRecyclerView.setAdapter(mEnterprisesAdapter);
                mEnterprisesAdapter.setRecyclerViewOnClickListener(new RecyclerViewOnClickListener() {
                    @Override
                    public void onClickListener(View view, int position) {
                        Intent intent = new Intent(getBaseContext(), DetailActivity.class);
                        intent.putExtra("ENTERPRISE_ID", enterprises.get(position).getId());
                        startActivity(intent);
                    }

                });

            }

            @Override
            public void onFailure(Call<EnterpriseSearchResponse> call, Throwable t) {
                mRvPlaceholder.setText("Ocorreu um erro! Verifique a conexão com a internet.");
            }


        });
    }

    private Map<String, String> getHeaders(Context context) {
        Map<String, String> map = new HashMap<>();

        String token = mSharedPreferences.getValueString(context, mSharedPreferences.TOKEN);
        String uid = mSharedPreferences.getValueString(context, mSharedPreferences.UID);
        String client = mSharedPreferences.getValueString(context, mSharedPreferences.CLIENT);
        map.put("Content-Type", "application/json");
        map.put("access-token", token);
        map.put("uid", uid);
        map.put("client", client);

        return map;
    }

    private class Search implements SearchView.OnQueryTextListener {

        @Override
        public boolean onQueryTextSubmit(String query) {
            connectAndGetApiData(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String query) {
            if (query.length() > 3) {
                connectAndGetApiData(query);
            }
            return false;
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(new Search());

        MenuItemCompat.setOnActionExpandListener(search,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {
                        mImgLogo.setVisibility(View.INVISIBLE);
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                        mImgLogo.setVisibility(View.VISIBLE);
                        return true;
                    }
                });

        return super.onCreateOptionsMenu(menu);
    }

}