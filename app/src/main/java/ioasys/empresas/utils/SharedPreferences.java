package ioasys.empresas.utils;

import android.content.Context;


public class SharedPreferences {
    //Shared preferences
    public static final String PREFS_NAME = "empresas.android";
    public static final String USER_AUTHENTICATED = "user_authenticated";
    public static final String TOKEN = "token";
    public static final String UID = "uid";
    public static final String CLIENT = "client";


    public void save(Context context, String value, String prefKey) {
        android.content.SharedPreferences settings;
        android.content.SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putString(prefKey, value);
        editor.commit();
    }

    public void save(Context context, boolean value, String prefKey) {
        android.content.SharedPreferences settings;
        android.content.SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putBoolean(prefKey, value);
        editor.commit();
    }


    public String getValueString(Context context, String prefKey) {
        android.content.SharedPreferences settings;
        String value = "";
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        value = settings.getString(prefKey, value);
        return value;
    }

    public boolean getValueBoolean(Context context, String prefKey) {
        android.content.SharedPreferences settings;
        boolean value;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        value = settings.getBoolean(prefKey, false);
        return value;
    }

    public void clearSharedPreference(Context context, String prefKey) {
        android.content.SharedPreferences settings;
        android.content.SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }


    public void removeValue(Context context, String prefKey) {
        android.content.SharedPreferences settings;
        android.content.SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.remove(prefKey);
        editor.commit();
    }
}
