package ioasys.empresas.rest;

import java.util.Map;

import ioasys.empresas.model.EnterpriseDetailResponse;
import ioasys.empresas.model.EnterpriseSearchResponse;
import ioasys.empresas.model.User;
import ioasys.empresas.model.UserResponse;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.Call;
import retrofit2.http.Query;

public interface EnterpriseApiService {


    @GET("enterprises/{id}")
    Call<EnterpriseDetailResponse> getEnterpriseDetails(@Path("id") int id,
                                                        @HeaderMap Map<String, String> headers);


    @GET("enterprises")
    Call<EnterpriseSearchResponse> searchEnterprisesName(@Query("name") String sort,
                                                         @HeaderMap Map<String, String> headers);

    @POST("users/auth/sign_in")
    Call<UserResponse> login(@Body User user);
}