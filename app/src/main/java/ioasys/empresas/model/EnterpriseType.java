package ioasys.empresas.model;

import com.google.gson.annotations.SerializedName;


public class EnterpriseType {

    @SerializedName("id")
    private Integer id;
    @SerializedName("enterprise_type_name")
    private String enterpriseTypeName;

    public EnterpriseType(Integer id, String enterpriseTypeName) {
        this.id = id;
        this.enterpriseTypeName = enterpriseTypeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnterpriseTypeName() {
        return enterpriseTypeName;
    }

    public void setEnterpriseTypeName(String enterpriseTypeName) {
        this.enterpriseTypeName = enterpriseTypeName;
    }
}
