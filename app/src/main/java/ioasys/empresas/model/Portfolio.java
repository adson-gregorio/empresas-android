package ioasys.empresas.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Portfolio {

    @SerializedName("enterprises_number")
    private String enterprisesNumber;
    @SerializedName("enterprises")
    private List<String> enterprises = null;

    public Portfolio(String enterprisesNumber, List<String> enterprises) {
        this.enterprisesNumber = enterprisesNumber;
        this.enterprises = enterprises;
    }

    public String getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(String enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<String> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<String> enterprises) {
        this.enterprises = enterprises;
    }

}