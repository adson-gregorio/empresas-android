package ioasys.empresas.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class EnterpriseDetailResponse {

    @SerializedName("enterprise")
    private Enterprise enterprise;
    @SerializedName("success")
    private Boolean success;

    public EnterpriseDetailResponse(Enterprise enterprise, Boolean success) {
        this.enterprise = enterprise;
        this.success = success;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
