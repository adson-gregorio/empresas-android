package ioasys.empresas.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Enterprise {

    @SerializedName("id")
    private Integer id;
    @SerializedName("enterprise_name")
    private String enterpriseName;
    @SerializedName("description")
    private String description;
    @SerializedName("email_enterprise")
    private String emailEnterprise;
    @SerializedName("facebook")
    private String facebook;
    @SerializedName("twitter")
    private String twitter;
    @SerializedName("linkedin")
    private String linkedin;
    @SerializedName("phone")
    private String phone;
    @SerializedName("own_enterprise")
    private String ownEnterprise;
    @SerializedName("photo")
    private String photo;
    @SerializedName("value")
    private Integer value;
    @SerializedName("shares")
    private Integer shares;
    @SerializedName("sharePrice")
    private Integer sharePrice;
    @SerializedName("ownShares")
    private Integer ownShares;
    @SerializedName("city")
    private String city;
    @SerializedName("country")
    private String country;
    @SerializedName("enterprise_type")
    private EnterpriseType enterpriseType;

    public Enterprise(Integer id,
                      String enterpriseName,
                      String description,
                      String emailEnterprise,
                      String facebook,
                      String twitter,
                      String linkedin,
                      String phone,
                      String ownEnterprise,
                      String photo,
                      Integer value,
                      Integer shares,
                      Integer sharePrice,
                      Integer ownShares,
                      String city,
                      String country,
                      EnterpriseType enterpriseType) {
        this.id = id;
        this.enterpriseName = enterpriseName;
        this.description = description;
        this.emailEnterprise = emailEnterprise;
        this.facebook = facebook;
        this.twitter = twitter;
        this.linkedin = linkedin;
        this.phone = phone;
        this.ownEnterprise = ownEnterprise;
        this.photo = photo;
        this.value = value;
        this.shares = shares;
        this.sharePrice = sharePrice;
        this.ownShares = ownShares;
        this.city = city;
        this.country = country;
        this.enterpriseType = enterpriseType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmailEnterprise() {
        return emailEnterprise;
    }

    public void setEmailEnterprise(String emailEnterprise) {
        this.emailEnterprise = emailEnterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOwnEnterprise() {
        return ownEnterprise;
    }

    public void setOwnEnterprise(String ownEnterprise) {
        this.ownEnterprise = ownEnterprise;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getShares() {
        return shares;
    }

    public void setShares(Integer shares) {
        this.shares = shares;
    }

    public Integer getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Integer sharePrice) {
        this.sharePrice = sharePrice;
    }

    public Integer getOwnShares() {
        return ownShares;
    }

    public void setOwnShares(Integer ownShares) {
        this.ownShares = ownShares;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }
}