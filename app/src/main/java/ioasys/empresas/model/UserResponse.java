package ioasys.empresas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("investor")
    private Investor investor;
    @SerializedName("enterprise")
    private String enterprise;
    @SerializedName("success")
    private boolean success;

    public UserResponse(Investor investor, boolean success, String enterprise) {
        this.investor = investor;
        this.success = success;
        this.enterprise = enterprise;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}