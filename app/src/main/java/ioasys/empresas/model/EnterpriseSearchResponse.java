package ioasys.empresas.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EnterpriseSearchResponse {

    @SerializedName("enterprises")
    private List<Enterprise> enterprises;


    public EnterpriseSearchResponse(List<Enterprise> enterprise) {
        this.enterprises = enterprises;
    }

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprise(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

}
