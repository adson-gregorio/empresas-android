package ioasys.empresas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Investor {

    @SerializedName("id")
    private String id;
    @SerializedName("investor_name")
    private String investorName;
    @SerializedName("email")
    private String email;
    @SerializedName("city")
    private String city;
    @SerializedName("country")
    private String country;
    @SerializedName("balance")
    private String balance;
    @SerializedName("photo")
    private String photo;
    @SerializedName("portfolio")
    private Portfolio portfolio;
    @SerializedName("portfolio_value")
    private String portfolioValue;
    @SerializedName("first_access")
    private String firstAccess;
    @SerializedName("super_angel")
    private String superAngel;

    public Investor(String id,
                    String investorName,
                    String email,
                    String city,
                    String country,
                    String balance,
                    String photo,
                    Portfolio portfolio,
                    String portfolioValue,
                    String firstAccess,
                    String superAngel) {
        this.id = id;
        this.investorName = investorName;
        this.email = email;
        this.city = city;
        this.country = country;
        this.balance = balance;
        this.photo = photo;
        this.portfolio = portfolio;
        this.portfolioValue = portfolioValue;
        this.firstAccess = firstAccess;
        this.superAngel = superAngel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public String getPortfolioValue() {
        return portfolioValue;
    }

    public void setPortfolioValue(String portfolioValue) {
        this.portfolioValue = portfolioValue;
    }

    public String getFirstAccess() {
        return firstAccess;
    }

    public void setFirstAccess(String firstAccess) {
        this.firstAccess = firstAccess;
    }

    public String getSuperAngel() {
        return superAngel;
    }

    public void setSuperAngel(String superAngel) {
        this.superAngel = superAngel;
    }

}