package ioasys.empresas.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ioasys.empresas.R;
import ioasys.empresas.model.Enterprise;

public class EnterprisesAdapter extends RecyclerView.Adapter<EnterprisesAdapter.MovieViewHolder> {

    private List<Enterprise> mEnterprises;
    private int mRowLayout;
    private Context mContext;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListener;


    public EnterprisesAdapter(List<Enterprise> enterprises, int rowLayout, Context context) {
        this.mEnterprises = enterprises;
        this.mRowLayout = rowLayout;
        this.mContext = context;
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView enterpriseName;
        TextView enterpriseType;
        TextView enterpriseCountry;
        ImageView enterpriseImage;

        public MovieViewHolder(View v) {
            super(v);
            enterpriseImage = (ImageView) v.findViewById(R.id.enterpriseImage);
            enterpriseName = (TextView) v.findViewById(R.id.enterpriseName);
            enterpriseType = (TextView) v.findViewById(R.id.enterpriseType);
            enterpriseCountry = (TextView) v.findViewById(R.id.enterpriseCountry);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRecyclerViewOnClickListener != null) {
                        mRecyclerViewOnClickListener.onClickListener(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public void setRecyclerViewOnClickListener(final RecyclerViewOnClickListener rvClickListener) {
        mRecyclerViewOnClickListener = rvClickListener;
    }


    @Override
    public EnterprisesAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mRowLayout, parent, false);
        return new MovieViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {

        Glide.with(mContext)
                .load(mEnterprises.get(position).getPhoto())
                .error(R.mipmap.img_lista)
                .into(holder.enterpriseImage);
        holder.enterpriseName.setText(mEnterprises.get(position).getEnterpriseName());
        holder.enterpriseType.setText(mEnterprises.get(position).getEnterpriseType().getEnterpriseTypeName());
        holder.enterpriseCountry.setText(mEnterprises.get(position).getCountry());
    }

    @Override
    public int getItemCount() {
        return mEnterprises.size();
    }
}