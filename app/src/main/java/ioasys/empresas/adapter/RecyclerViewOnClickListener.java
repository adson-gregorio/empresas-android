package ioasys.empresas.adapter;

import android.view.View;


public interface RecyclerViewOnClickListener {
    void onClickListener(View view, int position);
}
